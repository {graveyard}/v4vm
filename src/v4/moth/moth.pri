INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/qv4isel_moth_p.h \
    $$PWD/qv4instr_moth_p.h \
    $$PWD/qv4vme_moth_p.h

SOURCES += \
    $$PWD/qv4isel_moth.cpp \
    $$PWD/qv4instr_moth.cpp \
    $$PWD/qv4vme_moth.cpp

#DEFINES += DO_TRACE_INSTR
